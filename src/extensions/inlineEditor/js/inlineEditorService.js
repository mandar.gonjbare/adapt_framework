define([], function () {
  var baseUrl = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '');

  function updateText(url, data, callback) {

    $.ajax(baseUrl + url , {
      method: 'PUT',
      data: data
    }).done(function (result) {
      if (result) {
        callback(result);
      }
    }).fail(function (err) {
      console.log("Unable to update text - ", err);
    });
  }

  return {
    updateText: updateText
  };
})
