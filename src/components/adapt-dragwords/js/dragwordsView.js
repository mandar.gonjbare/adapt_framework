define([
    'core/js/adapt',
    'core/js/views/questionView',
    './dragableView',
    './dropdown'
], function (Adapt, QuestionView, DragableView, DropDown) {

    var DragwordsView = QuestionView.extend({
        dragableViews: [],
        isPlaced: false,
        dropdowns: null,
        showMyAnswer: true,

        setLayout: function () {

            if (this.model.get('_desktop')) {
                var dropAreas = this.$el.find(".drop-area");
                if (this.model.get("dragData") && this.model.get("dragData").length && this.dragableViews.length) {
                    for (var i = 0; i < this.model.get("dragData").length; i++) {
                        if (this.showMyAnswer) {
                            var dropId = this.model.get("dragData")[i].dropId;
                            if (dropId) {
                                const idx = this.dragableViews.findIndex(drag => drag.model.dropId === dropId);
                                this.dragableViews[idx].setToPosition(dropAreas[Number(dropId) - 1].offsetLeft, dropAreas[Number(dropId) - 1].offsetTop, false);
                            }
                        } else {
                            this.dragableViews[i].setToPosition(dropAreas[Number(this.model.get("dragData")[i].correctDropId) - 1].offsetLeft, dropAreas[Number(this.model.get("dragData")[i].correctDropId) - 1].offsetTop, false);
                        }

                    }
                }
            }
        },

        resetQuestionOnRevisit: function () {
            this.setAllItemsEnabled(true);
            this.resetQuestion();
        },

        disableQuestion: function () {
            this.setAllItemsEnabled(false);
        },

        enableQuestion: function () {
            this.setAllItemsEnabled(true);
        },

        setAllItemsEnabled: function (isEnabled) {
            if (isEnabled) {
                if (this.model.get('_desktop')) {
                    this.$('.dragwords-widget').removeClass('dragwords-widget-disabled');
                } else {
                    this.dropdowns.forEach(function (dropdown) {
                        dropdown.toggleDisabled(false);
                    });
                }
            } else {
                if (this.model.get('_desktop')) {
                    this.$('.dragwords-widget').addClass('dragwords-widget-disabled');
                } else {
                    this.dropdowns.forEach(function (dropdown) {
                        dropdown.toggleDisabled(true);
                    });
                }
            }
        },

        // Used by question to setup itself just after rendering
        onQuestionRendered: function () {
            this.setReadyStatus();
            // this.listenTo(Adapt, 'device:changed', this.render);
            this.listenTo(this.model, {
                "Question:show-model-answer": this.onShowCorrectAnswerClicked,
                "Question:hide-model-answer": this.onHideCorrectAnswerClicked
            });
            if (this.model.get('_desktop') || Adapt.device.screenSize == 'large' || Adapt.device.screenSize == 'medium') {
                console.log("using the drag items");
                this.setupDragAndDrop();
                this.listenTo(Adapt, 'device:resize', this.setLayout);
            } else {
                this.setUpDropdowns();
                this.listenToOnce(Adapt.parentView, 'postRemove', this.onPostRemove);

            }

            if (this.model.get("_isSubmitted")) {
                console.log("calling hide correct answer");
                this.hideCorrectAnswer();
                this.showMarking();
                this.setAllItemsEnabled(false);
            }
        },

        onPostRemove: function () {
            this.dropdowns.forEach(function (dropdown) {
                dropdown.off('change', this.onOptionSelected);
                dropdown.destroy();
            }, this);
        },

        setUpDropdowns: function () {
            _.bindAll(this, 'onOptionSelected');
            this.dropdowns = [];
            var items = this.model.get('_items');
            this.$('.matching__item').each(function (i, el) {
                var item = items[i];
                var selectedOption = _.find(item._options, function (option) {
                    return option._isSelected;
                });
                var value = selectedOption ? selectedOption._index : null;
                var dropdown = new DropDown({
                    el: $(el).find('.dropdown')[0],
                    placeholder: this.model.get('placeholder'),
                    value: value
                });
                this.dropdowns.push(dropdown);
                dropdown.on('change', this.onOptionSelected);
            }.bind(this));
            this.enableQuestion();
            if (this.model.get('_isEnabled') !== true) {
                this.disableQuestion();
            }
        },

        onOptionSelected: function (dropdown) {
            if (this.model.get('_isInteractionComplete')) return;
            var $container = dropdown.$el.parents('.matching__select-container');
            $container.removeClass('error');
            var itemIndex = dropdown.$el.parents('.matching__item').index();
            if (dropdown.isEmpty()) return;
            var optionIndex = parseInt(dropdown.val());
            this.model.setOptionSelected(itemIndex, optionIndex, true);
        },

        setupDragAndDrop: function () {
            var self = this;
            console.log(this.model.get("dragData"));
            if (this.model.get("dragData") && this.model.get("dragData").length) {
                console.log("generating dragable view");
                this.dragableViews = _.map(this.model.get("dragData"), function (dragItem, i) {
                    const newDragableView = new DragableView({ model: dragItem });
                    newDragableView.$el.attr('data-id', 'drag-' + dragItem.id);
                    newDragableView.$el.attr('id', dragItem.id);

                    this.$el.find(".dragwords-widget").append(newDragableView.$el);

                    return newDragableView;
                }.bind(this));
                console.log("dragable view", this.dragableViews);
                // for (var i = 0; i < this.model.get("dragData").length; i++) {

                //     const newDragableView = new DragableView({ model: this.model.get("dragData")[i] });
                //     newDragableView.$el.attr('data-id', 'drag-' + this.model.get("dragData")[i].id);
                //     newDragableView.$el.attr('id', this.model.get("dragData")[i].id);

                //     this.$el.find(".dragwords-widget").append(newDragableView.$el);
                //     this.dragableViews.push(newDragableView);
                // }

                Adapt.on('dragwords:drop', function (evt) {
                    self.checkDrop(evt.event.target, evt.doppedElement);
                });
            }

            if (this.model.get("dropData") && this.model.get("dropData").length) {
                for (var i = 0; i < this.model.get("dropData").length; i++) {
                    var left = this.model.get("dropData")[i].left + '%';
                    var top = this.model.get("dropData")[i].top + '%';
                    this.$el.find(".drop-area").css({ position: 'absolute' });
                    this.$el.find(`.drop-area-${i}`).css({ top: top, left: left });
                }
            }
        },

        checkDrop: function (doppedElement, newDragableView) {
            var dragRect = doppedElement.getBoundingClientRect();

            var self = this;
            var dropAreas = this.$el.find(".drop-area");
            // var isPlaced = false;
            var dragableDropId = newDragableView.model.dropId;
            var dragId = newDragableView.model.id;
            const index = this.model.get("dragData").findIndex(drag => drag.id === dragId);

            for (var i = 0; i < dropAreas.length; i++) {
                var dropRect = dropAreas[i].getBoundingClientRect();
                var dropId = $(dropAreas[i]).attr('data-itemId');

                if (this.isIntersect(dragRect, dropRect)) {
                    if (!dropAreas[i].isFilled) {
                        if (dragableDropId) {
                            newDragableView.setToPosition(dropAreas[i].offsetLeft, dropAreas[i].offsetTop, false);

                            this.model.get("dragData")[index].isHome = false;
                            this.model.get("dragData")[index].dropId = dropId;
                            dropAreas[i].isFilled = true;
                            dropAreas[Number(dragableDropId) - 1].isFilled = false;
                            this.isPlaced = true;
                            break;
                        } else {
                            newDragableView.setToPosition(dropAreas[i].offsetLeft, dropAreas[i].offsetTop, false);

                            this.model.get("dragData")[index].isHome = false;
                            this.model.get("dragData")[index].dropId = dropId;
                            dropAreas[i].isFilled = true;
                            this.isPlaced = true;
                            break;
                        }
                    }
                    else {
                        const idx = this.dragableViews.findIndex(drag => drag.model.dropId === dropId);

                        if (idx > -1) {
                            this.dragableViews[idx].gotoHome();
                        }
                        newDragableView.setToPosition(dropAreas[i].offsetLeft, dropAreas[i].offsetTop, false);
                        this.model.get("dragData")[index].isHome = false;
                        this.model.get("dragData")[index].dropId = dropId;
                        dropAreas[i].isFilled = true;
                        this.isPlaced = true;
                        if (dragableDropId) {
                            dropAreas[Number(dragableDropId) - 1].isFilled = false;
                        }
                        if (dragableDropId === dropId) {
                            dropAreas[i].isFilled = false;
                            this.isPlaced = false;
                        }
                    }
                }
                // if (dragableDropId === dropId) {
                //     dropAreas[i].isFilled = false;
                //     this.isPlaced = false;
                // }
            }

            if (!this.isPlaced) {
                newDragableView.gotoHome();
            }
            this.model.checkCanSubmit();
        },

        isIntersect: function (rect1, rect2) {
            return (rect1.left < rect2.right && rect1.right > rect2.left &&
                rect1.top < rect2.bottom && rect1.bottom > rect2.top);
        },

        onCannotSubmit: function () {
            if (!this.model.get('_desktop')) {
                this.dropdowns.forEach(function (dropdown) {
                    if (!dropdown.isEmpty()) return;
                    dropdown.$el.parents('.matching__select-container').addClass('has-error');
                });
            }
        },

        showMarking: function () {
            if (!this.model.get('_canShowMarking')) return;
            if (this.model.get('_desktop')) {
                _.each(this.model.get('dropData'), function (item, index) {
                    var $item = this.$el.find(`.drop-area-${index}`);
                    $item.removeClass('correct incorrect');
                    $item.addClass(item._isCorrect ? 'correct' : 'incorrect');
                }, this);
            } else {
                this.model.get('_items').forEach(function (item, i) {
                    var $item = this.$('.matching__item').eq(i);
                    $item.removeClass('is-correct is-incorrect').addClass(item._isCorrect ? 'is-correct' : 'is-incorrect');
                }, this);
            }
        },

        resetQuestion: function () {
            if (this.model.get('_desktop')) {
                var dropAreas = this.$el.find(".drop-area");

                _.each(this.dragableViews, function (item, index) {
                    dropAreas[index].isFilled = false;
                    this.isPlaced = false;
                    item.gotoHome();
                }, this);
            } else {
                this.$('.matching__item').removeClass('is-correct is-incorrect');
                this.model.set('_isAtLeastOneCorrectSelection', false);
                var resetAll = this.model.get('_shouldResetAllAnswers');

                this.model.get('_items').forEach(function (item, index) {
                    if (item._isCorrect && resetAll === false) return;
                    this.selectValue(index, null);
                    item._options.forEach(function (option, index) {
                        option._isSelected = false;
                    });
                    item._selected = null;
                }, this);
            }
        },

        showCorrectAnswer: function () {
            if (this.model.get('_desktop')) {
                this.showMyAnswer = false;
                var dropAreas = this.$el.find(".drop-area");

                _.each(this.model.get('dragData'), function (item, index) {
                    this.dragableViews[index].setToPosition(dropAreas[Number(item.correctDropId) - 1].offsetLeft, dropAreas[Number(item.correctDropId) - 1].offsetTop, true);
                    $(dropAreas[index]).removeClass("incorrect").addClass("correct");
                }, this);

            } else {
                this.model.get('_items').forEach(function (item, index) {
                    var correctOption = _.findWhere(item._options, { _isCorrect: true });
                    this.selectValue(index, correctOption._index);
                }, this);
            }
        },

        hideCorrectAnswer: function () {
            if (this.model.get('_desktop')) {
                var dropAreas = this.$el.find(".drop-area");
                // if(!this.dragableViews.length){
                //     this.model.set
                // }
                _.each(this.model.get('dragData'), function (item, index) {
                    if (item.dropId) {
                        this.dragableViews[index].setToPosition(dropAreas[Number(item.dropId) - 1].offsetLeft, dropAreas[Number(item.dropId) - 1].offsetTop, true);
                        var dropItem = this.model.getDropItemById(item.dropId);
                        if (dropItem) {
                            dropItem._isCorrect ? $(dropAreas[index]).removeClass("correct incorrect").addClass("correct") :
                                $(dropAreas[index]).removeClass("correct incorrect").addClass("incorrect");
                        }
                    }
                }, this);
                this.showMyAnswer = true;
            } else {
                var answerArray = this.model.has('_tempUserAnswer') ?
                    this.model.get('_tempUserAnswer') :
                    this.model.get('_userAnswer');

                this.model.get('_items').forEach(function (item, index) {
                    var key = answerArray[index];
                    var value = item._options[key]._index;
                    this.selectValue(index, value);
                }, this);
            }
        },

        selectValue: function (index, optionIndex) {
            if (!this.dropdowns) return;
            var dropdown = this.dropdowns[index];
            if (!dropdown) return;
            dropdown.select(optionIndex);
        },

        // disableButtonActions: function (val) {
        //     this.$(".buttons-action").prop("disabled", val);
        // },
    });

    return DragwordsView;

});
