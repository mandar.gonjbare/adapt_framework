define(function (require) {
  var Adapt = require("coreJS/adapt");
  var InlineEditorView = require('extensions/inlineEditor/js/inlineEditorView');
  // window.io = require('../lib/socket.io.min');

  Adapt.once('configModel:dataLoaded', onConfigLoaded);

  function onConfigLoaded() {
    if (!Adapt.config.has('_inlineEditor')) return;
    if (!Adapt.config.get('_inlineEditor')._isEnabled) return;

    Adapt.once("app:dataReady", onDataReady);
  }

  function onDataReady() {

    // var views = ["contentObject", "article", "block", "component"];

    // Adapt.on(views.join("View:postRender ") + "View:postRender", function (view) {
    //   createView(view);
    // });

    Adapt.on("contentObjectView:postRender", function (view) {
      createView(view);
    });

    function createView(view) {
      const newInlineEditorView = new InlineEditorView({ model: view.model });
      $('body').append(newInlineEditorView.$el);
      // view.$el.append(newInlineEditorView.$el);

    };

  };

});
