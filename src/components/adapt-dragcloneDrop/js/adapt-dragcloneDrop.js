define([
  'core/js/adapt',
  './dragcloneDropView',
  './dragcloneDropModel'
], function(Adapt, DragcloneDropView, DragcloneDropModel) {

  return Adapt.register('dragcloneDrop', {
    view: DragcloneDropView,
    model: DragcloneDropModel
  });
});

