define(function (require) {

  var Adapt = require('coreJS/adapt');
  var Backbone = require('backbone');
  var InlineEditorService = require('extensions/inlineEditor/js/inlineEditorService');
  var BalloonEditor = require('../lib/ckeditor');
  // var ClassicEditor = require('../lib/ckeditor');
  // var InlineEditor = require('../lib/ckeditor');
  var editables = null;
  var CKEditorArray = [];
  var isEditable = false;

  Adapt.on("contentObjectView:postRemove", function (view) {
    isEditable = false;
  });

  var InlineEditorView = Backbone.View.extend({
    tagName: 'div',

    className: 'edit-preview icon icon-pencil',

    events: {
      'click': 'toggleEdit'
    },

    initialize: function () {
      this.render();
    },

    render: function () {
      this.id = this.model.get("_id");
      this.type = this.model.get("_type");
      this.$el.attr('id', this.id);
      return this;
    },

    toggleEdit: function (event) {
      if (isEditable) {
        isEditable = false;
        this.resetEdit(event);
      } else {
        isEditable = true;
        this.contentEdit(event);
      }
    },

    contentEdit: function (event) {
      event.preventDefault();
      event.stopPropagation();
      var parentNode = $(event.currentTarget).siblings('#app')[0];

      if (this.model.get("_type") === 'course') {
        var cc = $(parentNode).find('.course, .boxmenu')[0];
        var courseId = $(cc).attr('data-adapt-id');
        // editables = $(cc).find('.menu__title-inner, .menu__body-inner', '.menu-item__title-inner', '.menu-item__body-inner');
        editables = $(cc).find('.menu__title-inner, .menu__body-inner, .menu-item__title-inner, .menu-item__body-inner');
        this.setEdit(event, editables, courseId);
      } else {
        var cc = $(parentNode).find(`.${this.type}`)[0];
        var pageId = $(cc).attr('data-adapt-id');
        editables = $(cc).find(".page__title-inner, .page__body-inner, .page__instruction-inner, .article__title-inner, .article__body-inner, .article__instruction-inner, .block__title-inner, .block__body-inner, .block__instruction-inner, .component__title-inner, .component__body-inner, .component__instruction-inner");
        this.setEdit(event, editables, pageId);
      }
    },

    setEdit: function (event, editables, parentId) {
      var self = this;

      if (editables && editables.length) {
        for (var i = 0; i < editables.length; i++) {
          $(editables[i]).prop('contenteditable', true);
          // (editables[i]).focus();
          this.CKEditorInit(editables[i]);

          $(editables[i]).on('blur', function (e) {
            e.preventDefault();
            e.stopPropagation();

            // var typeArray = ["page", "article", "block", "component"];
            var elementClass = $(e.currentTarget).attr('class').split('-inner');
            var element = elementClass[0];
            var elementType = element.split('__')[0] === 'menu' ? 'course' : element.split('__')[0];
            var elementPropertyChanged = element.split('__')[1];
            var clickedElementId = $(e.currentTarget).parent().parent().parent().parent().parent().attr('data-adapt-id');

            self.saveChanges(clickedElementId, elementType, elementPropertyChanged, e.currentTarget.innerHTML);
          });
        }
      }
    },

    //initialize function
    CKEditorInit: function (selector_element) {
      var config = {
        toolbar: ['bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote', 'undo', 'redo'],
      };

      BalloonEditor
        .create(selector_element, config)
        .then(editor => {
          CKEditorArray.push(editor);//save editor with selector name as index to array
        })
        .catch(error => {
          console.error(error);
        });
    },

    resetEdit: function (event) {

      if (editables && editables.length) {
        for (var i = 0; i < editables.length; i++) {
          if ($(editables[i]).length) {
            CKEditorArray[i].destroy();
            $(editables[i]).prop('contenteditable', false);
            $(editables[i]).off('blur');
          }
        }
        editables = [];
        CKEditorArray = [];
      }
    },

    remove_tag: function (text, tag) {
      const regex = new RegExp(`^<${tag}>|</${tag}>$`, 'g');
      return text.replace(regex, "").trim();
    },

    saveChanges: function (id, type, property, value) {
      console.log(type)

      var data = {};
      var url = window.location.href.split("/");
      var courseId = url[url.length - 1] === '' ? url[url.length - 2] : url[url.length - 1];

      var id = type === 'course' ? courseId : id;
      var type = (type === 'page') || (type === 'menu-item') ? 'contentobject' : type;

      data[property] = this.remove_tag(value.replace(/[\r\n]+/gm, "").trim(), "p");
      if (property === 'title') {
        data['displayTitle'] = this.remove_tag(value.replace(/[\r\n]+/gm, "").trim(), "p");
      }

      if (property === 'body' && type === 'contentobject') {
        data['pageBody'] = this.remove_tag(value.replace(/[\r\n]+/gm, "").trim(), "p");
      }

      // console.log(`/api/content/${type}/${id}`)
      // console.log(data)

      //   if(this.model.get(property) != value.replace(/[\r\n]+/gm, "").trim()) {
      //   const socket = window.io.connect('http://localhost:3000/');
      //   socket.on('connect', () => {
      //     // console.log('Successfully connected!', socket.connected);
      //     socket.emit('chat message', data);
      //   });

      //   socket.on('chat message', function (msg) {
      //     // console.log('msg', msg);
      //     socket.disconnect();
      //   });

      //   socket.on("disconnect", () => {
      //     console.log(socket.connected); // false
      //   });
      // }

      InlineEditorService.updateText(`/api/content/${type}/${id}`, data, function (result) {
        console.log("result", result);
      });
    }

  });
  return InlineEditorView;

});
