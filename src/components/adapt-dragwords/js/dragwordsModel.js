import Adapt from 'core/js/adapt';
import QuestionModel from 'core/js/models/questionModel';

export default class DragwordsModel extends QuestionModel {

  init() {
    QuestionModel.prototype.init.call(this);
    if (Adapt.device.screenSize == 'large' || Adapt.device.screenSize == 'medium') {
      this.set('_desktop', true);
      this.setupDragAndDropItems();
    } else {
      this.set('_desktop', false);
      this.setupMatchingItems();
    }
  }

  setupMatchingItems() {

    if (!this.get("_isSubmitted")) {
      var _items = [];

      _.each(this.get("_drops"), function (item, index) {
        var matchingOptions = [];
        _.each(this.get("_drags"), function (option, i) {
          var _isCorrect = item.dropId === Number(option["correctDropId"]) ? true : false;
          matchingOptions.push({ _index: i, id: option["dragId"], text: option["text"], _isCorrect: _isCorrect, _isSelected: false });
        }, this);
        _items.push({ _index: index, id: item["dropId"], text: item["text"], _selected: false, _options: matchingOptions });
        matchingOptions = [];
      }, this);

      if (this.get("_isRandom") && this.get("_isEnabled")) {
        // _items = _.shuffle(_items);
        _items.forEach(function (item) {
          item._options = _.shuffle(item._options);
        });
      }
      this.set("_items", _items);
      return;
    }
  }

  setupDragAndDropItems() {

    // if (!this.get("_isSubmitted")) {
    // var dragData = [];
    // var dropData = [];
    var left = 0;
    var top = 130;

    var dragData = _.map(this.get("_drags"), function (item, index) {
      var isHome = item.isHome === undefined ? true : item.isHome;
      var dropId = item.dropId === undefined ? null : item.dropId;
      var newDrag = { id: item["dragId"], title: item["text"], l: left, t: top, isHome: isHome, dropId: dropId, correctDropId: item["correctDropId"] };
      top += 64;
      return newDrag;
    }, this);

    var dropData = _.map(this.get("_drops"), function (item, index) {
      return { id: item["dropId"], option: item["text"], left: item["left"], top: item["top"] };
    }, this);

    // if (this.get("_isRandom") && this.get("_isEnabled")) {
    //   dragData = _.shuffle(dragData);
    //   dropData = _.shuffle(dropData);
    // }

    this.set("dragData", dragData);
    this.set("dropData", dropData);
    return;
    // }
  }

  getDragItemById(id) {
    return _.filter(this.get("dragData"), function (dragItems) {
      return id == dragItems['id'];
    }, this)[0];
  }

  //Pending
  restoreUserAnswers() {

    if (!this.get("_isSubmitted")) return;

    if (this.get('_desktop')) {
      this.setupDragAndDropItems();
      var userAnswer = this.get("_userAnswer");
      _.each(userAnswer, function (item, index) {
        var dragItem = this.getDragItemById(index + 1);
        // dragItem.dropId = (index + 1).toString();
        dragItem.dropId = userAnswer[index].toString();
        dragItem.isHome = false;
      }, this);

    } else {
      var userAnswer = this.get('_userAnswer');

      this.get('_items').forEach(function (item, index) {
        item._options.forEach(function (option, index) {
          if (option._index === userAnswer[item._index]) {
            option._isSelected = true;
            item._selected = option;
          }
        });
      });
    }

    this.setQuestionAsSubmitted();
    this.checkCanSubmit();
    this.markQuestion();
    this.setScore();
    this.setupFeedback();
  }

  canSubmit() {
    if (this.get('_desktop')) {
      if (!this.get("dragData")) return false;

      var options = _.filter(this.get("dragData"), function (option) {
        return option.dropId != null;
      });

      var canSubmit = options.length === this.get("dragData").length;

    } else {
      var canSubmit = _.every(this.get('_items'), function (item) {
        return _.findWhere(item._options, { '_isSelected': true }) !== undefined;
      });
    }
    return canSubmit;

  }

  storeUserAnswer() {
    if (this.get('_desktop')) {
      var userAnswer = [];
      _.each(this.get('dragData'), function (item, index) {
        userAnswer[index] = Number(item.dropId);
        // userAnswer.push([
        //   Number(item.dropId)
        // ]);
      }, this);
      this.set('_userAnswer', userAnswer);

    } else {
      var userAnswer = new Array(this.get('_items').length);
      var tempUserAnswer = new Array(this.get('_items').length);

      this.get('_items').forEach(function (item, index) {
        var optionIndex = _.findIndex(item._options, function (o) { return o._isSelected; });

        tempUserAnswer[item._index] = optionIndex;
        userAnswer[item._index] = item._options[optionIndex]._index;
      }, this);

      this.set({
        _userAnswer: userAnswer,
        _tempUserAnswer: tempUserAnswer
      });
    }
  }

  setOptionSelected(itemIndex, optionIndex, isSelected) {
    var item = this.get('_items')[itemIndex];
    var option = _.findWhere(item._options, { '_index': optionIndex });
    option._isSelected = isSelected;
    item._selected = option;
    this.checkCanSubmit();
  }

  isCorrect() {

    var numberOfCorrectAnswers = 0;
    if (this.get('_desktop')) {
      _.each(this.get('dragData'), function (item, index) {

        if (item.dropId != null && item.correctDropId === item.dropId) {
          numberOfCorrectAnswers++;
          item._isCorrect = true;
          var dropItem = this.getDropItemById(item.dropId);
          if (dropItem) {
            dropItem['_isCorrect'] = true;
          }
          this.set('_numberOfCorrectAnswers', numberOfCorrectAnswers);
          this.set('_isAtLeastOneCorrectSelection', true);
        } else {
          item._isCorrect = false;
          var dropItem = this.getDropItemById(item.dropId);
          if (dropItem) {
            dropItem['_isCorrect'] = false;
          }
        }
      }, this);

      return numberOfCorrectAnswers === this.get('dragData').length;

    } else {

      this.get('_items').forEach(function (item, index) {

        var isCorrect = (item._selected && item._selected._isCorrect);
        if (!isCorrect) {
          item._isCorrect = false;
          return;
        }
        numberOfCorrectAnswers++;
        item._isCorrect = true;
        this.set({
          _numberOfCorrectAnswers: numberOfCorrectAnswers,
          _isAtLeastOneCorrectSelection: true
        });

      }, this);

      this.set('_numberOfCorrectAnswers', numberOfCorrectAnswers);
      return numberOfCorrectAnswers === this.get('_items').length;
    }
  }

  getDropItemById(id) {
    return _.filter(this.get("dropData"), function (dropItems) {
      return id == dropItems['id'];
    }, this)[0];
  }

  setScore() {
    var numberOfCorrectAnswers = this.get("_numberOfCorrectAnswers") || 0;
    var questionWeight = this.get("_questionWeight");

    if (this.get('_isCorrect')) {
      this.set('_score', questionWeight);
      return;
    }

    var itemLength = this.get('_desktop') ? this.get("dragData").length : this.get('_items').length;
    var score = (questionWeight * numberOfCorrectAnswers) / itemLength;

    this.set("_score", score);
  }

  isPartlyCorrect() {
    return this.get('_isAtLeastOneCorrectSelection');
  }

  resetUserAnswer() {
    this.set('_userAnswer', []);
  }

  getResponse() {
    var responses = [];
    if (this.get('_desktop')) {
      this.get('dragData').forEach(function (item, index) {
        responses.push((index + 1) + "." + Number(+item.dropId));
      }.bind(this));

      return responses.join(',');
    } else {
      this.get('_userAnswer').forEach(function (userAnswer, index) {
        responses.push((index + 1) + '.' + (userAnswer + 1));
      });

      return responses.join('#');
    }
  }

  getResponseType() {
    return "matching";
  }
}