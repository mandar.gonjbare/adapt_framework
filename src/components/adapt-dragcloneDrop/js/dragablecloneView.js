define(function (require) {
  var Adapt = require("core/js/adapt");

  var DragablecloneView = Backbone.View.extend({
    tagName: 'div',
    className: 'dragcloneDrop-answer',
    dragElement: null,
    originalBound: null,
    parentBoundArea: null,
    targetObj: null,

    initialize: function () {
      this.render();
    },

    render() {
      var self = this;

      var template = Handlebars.templates['dragableclone'];
      this.$el.html(template(this.model));

      this.dragElement = this.$el[0];

      this.setToPosition(this.model.l, this.model.t);
      this.dragElement.style.zIndex = 98;
      // var cloneElement1 = $(this.$el).clone(true ).appendTo('.dragcloneDrop-widget');
      // var cloneElement1 = $(this.$el).clone(true );
      // console.log(cloneElement1); 

      // var  divClone;
      // for(var i=0;i<4;i++)
      // {
      //   divClone = this.dragElement.cloneNode(true ); // the true is for deep cloning
      //   console.log(divClone)
      //   $('.dragcloneDrop-widget').append(divClone);
      // }

      // cloneElement1[0].addEventListener('mousedown', function (e) {
      //   self.startDrag(e, self);
      // });


      this.dragElement.addEventListener('mousedown', function (e) {
        self.startDrag(e, self);
      });

      this.originalBound = this.dragElement.getBoundingClientRect();
      var parent = $('.dragcloneDrop-widget')[0];
      this.parentBoundArea = { x: parent.offsetLeft, y: parent.offsetTop, w: parent.offsetWidth, h: parent.offsetHeight };
    },

    gotoHome: function () {
      this.dragElement.style.left = this.model.l + 'px';
      this.dragElement.style.top = this.model.t + 'px';
      this.model.isHome = true;
      this.model.dropId = null;
      this.model._isCorrect = false;
      $(this.dragElement).addClass('reset-animation');
    },

    getToPosition: function () {
      return {
        left: this.dragElement.style.left,
        top: this.dragElement.style.top
      };
    },

    setToPosition: function (left, top, show) {
      this.dragElement.style.left = left + 'px';
      this.dragElement.style.top = top + 'px';
      this.dragElement.style.zIndex = 98;
      if (show) {
        $(this.dragElement).addClass('reset-animation');
      }
    },

    getTarget: function () {
      return targetObj;
    },

    setTarget: function (target) {
      targetObj = target;
    },

    startDrag: function (event, self) {

      self.dragElement.style.zIndex = 99;
      $(self.dragElement).removeClass('reset-animation');

      var scroll = self.getScrollOffsets();  // A utility function from elsewhere
      var startX = event.clientX + scroll.x;
      var startY = event.clientY + scroll.y;

      var origX = self.dragElement.offsetLeft;
      var origY = self.dragElement.offsetTop;

      var deltaX = startX - origX;
      var deltaY = startY - origY;

      document.addEventListener("mousemove", moveHandler, true);
      document.addEventListener("mouseup", upHandler, true);

      if (event.stopPropagation) event.stopPropagation();  // Standard model
      else event.cancelBubble = true;                      // IE

      // Now prevent any default action.
      if (event.preventDefault) event.preventDefault();   // Standard model
      else event.returnValue = false;                     // I


      function moveHandler(e) {
        if (!e) e = window.event;  // IE event Model

        var scroll1 = self.getScrollOffsets();
        var left = e.clientX + scroll1.x - deltaX;
        var top = e.clientY + scroll1.y - deltaY;

        if (left < self.parentBoundArea.x) {
          left = self.parentBoundArea.x;
        }

        if (top < (self.parentBoundArea.y)) {
          top = self.parentBoundArea.y;
        }

        var right = left + 150;
        var bottom = top + 60;

        if (right > self.parentBoundArea.w) {
          left = self.parentBoundArea.w - 150;
        }

        if (bottom > self.parentBoundArea.h) {
          top = self.parentBoundArea.h - 60;
        }

        self.dragElement.style.left = left + "px";
        self.dragElement.style.top = top + "px";

        // And don't let anyone else see this event.
        if (e.stopPropagation) e.stopPropagation();  // Standard
        else e.cancelBubble = true;                  // IE
      }


      function upHandler(e) {
        if (!e) e = window.event;  // IE Event Model

        if (document.removeEventListener) {  // DOM event model
          document.removeEventListener("mouseup", upHandler, true);
          document.removeEventListener("mousemove", moveHandler, true);
        }
        else if (document.detachEvent) {  // IE 5+ Event Model
          // dragElement.detachEvent("onlosecapture", upHandler);
          // dragElement.detachEvent("onmouseup", upHandler);
          // dragElement.detachEvent("onmousemove", moveHandler);
          // dragElement.releaseCapture();
        }

        // And don't let the event propagate any further.
        if (e.stopPropagation) e.stopPropagation();  // Standard model
        else e.cancelBubble = true;                  // IE

        // Trigger on drop event
        Adapt.trigger('dragcloneDrop:drop', { event: e, doppedElement: self });

      }
    },

    getScrollOffsets: function () {

      var top = window.pageYOffset || document.documentElement.scrollTop,
        left = window.pageXOffset || document.documentElement.scrollLeft;

      return {
        x: left, y: top
      };
    },

  })
  return DragablecloneView;
})